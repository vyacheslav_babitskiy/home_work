import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.IntStream;


public class Parser {

    static String[] str = null;
    static int[] arr;

    private static String[] readFromFile() {
        try {
            str = new BufferedReader(new FileReader("./src/data.txt")).readLine().split(",");
        } catch (IOException e) {
            System.out.println("Log: " + e.getMessage());
        }
        return str;
    }

    private static int[] convertToIntArray() {
        return arr = Arrays.stream(readFromFile()).mapToInt(Integer::parseInt).toArray();
    }

    public static void sortArray() {
        convertToIntArray();
        IntStream.range(0, arr.length).map(i -> i++).forEach(System.out::println);
    }

    public static void reverseArray() {
        convertToIntArray();
        IntStream.range(0, arr.length).map(i -> arr.length - i - 1).forEach(System.out::println);
    }
}
